from django.db import models

# Create your models here.


class Urls(models.Model):
    corta = models.URLField()
    original = models.URLField()
