#Create your views here.
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from .models import Urls
import random

@csrf_exempt
def index(request):
    urls_list = Urls.objects.all()
    #Si el metodo es POST
    if request.method == "POST":
        url = request.POST['url']
        if (url[:4] != 'http'): #añado en caso de que no http la url
            new_url = ("https://" + url)
        else:
            new_url = url
        try:
            url_ = Urls.objects.get(original=new_url)
            context = {'url': url_.new_url}
            return render(request, 'acorta/redirect.html', context, status=301)
        except Urls.DoesNotExist:
            shorturl = random.randint(1,9999)
            c = Urls(original=new_url, corta=shorturl)
            c.save()
            urls_list = Urls.objects.all()
            context = {'urls_list': urls_list}
        return render(request, 'acorta/content.html', context)
    #Si el metodo es GET
    elif request.method == "GET":
        context = {'urls_list': urls_list}
        return render(request, 'acorta/content.html', context)


@csrf_exempt
def redirect(request, corta):
    if request.method == "GET":
        try:
            url_corta = Urls.objects.get(corta=corta)
            context = {'url': url_corta.original}
            return render(request, 'acorta/redirect.html', context, status=301)
        except Urls.DoesNotExist:
            urls_list = Urls.objects.all()
            context = {'urls_list': urls_list}
            return render(request, 'acorta/error.html', context, status=404)